import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.gl2.GLUT;        
import com.jogamp.opengl.GL;
import static com.jogamp.opengl.GL.GL_DEPTH_TEST;
import static com.jogamp.opengl.GL.GL_LEQUAL;
import static com.jogamp.opengl.GL.GL_LINES;
import static com.jogamp.opengl.GL.GL_TEXTURE_2D;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import static com.jogamp.opengl.fixedfunc.GLLightingFunc.GL_SMOOTH;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.GL_MODELVIEW;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.GL_PROJECTION;
import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

class Light extends GLCanvas implements GLEventListener, KeyListener {

   private static String TITLE = "Aplicacion de iluminacion";  // window's title
   private static final int CANVAS_WIDTH = 1800;  // width of the drawable
   private static final int CANVAS_HEIGHT = 1000; // height of the drawable
   private static final int FPS = 60; // animator's target frames per second
   private static final float factInc = 5.0f; // animator's target frames per second
   private float fovy = 45.0f;    
    
  //////////////// Variables /////////////////////////
 
  
  // Referencias de rotacion
  float rotacion=0.0f;
  float rotX = 90.0f;
  float rotY = 0.0f;
  float rotZ = 0.0f;
   
  // Posicion de la luz.
  float lightX=2f;
  float lightY=2f;
  float lightZ=2f;
  float dLight=.5f;

  // Material y luces.
  final float ambient[] = { 0.0f, 0.0f, 0.0f, 1.0f };
  final float position[] = { lightX, lightY, lightZ, 1.0f };
  final float mat_diffuse[] = { 0.6f, 0.6f, 0.6f, 1.0f };
  final float mat_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
  final float mat_shininess[] = { 50.0f };

//Sol
final float[] sol_ambient ={ 0.329412f, 0.223529f, 0.027451f,1.0f };
final float[] sol_diffuse ={ 0.780392f, 0.568627f, 0.113725f, 1.0f };
final float[] sol_specular ={ 0.992157f, 0.941176f, 0.807843f, 1.0f };
final float sol_shine[] ={51.2f} ;

//Mercurio
final float[] mer_ambient ={ 0.19125f, 0.0735f, 0.0225f, 1.0f };
final float[] mer_diffuse ={0.7038f, 0.27048f, 0.0828f, 1.0f };
final float[] mer_specular ={0.256777f, 0.137622f, 0.086014f, 1.0f };
final float mer_shine[] = {12.8f};

//venus
final float[] ven_ambient ={ 0.329412f, 0.223529f, 0.027451f,1.0f };
final float[] ven_diffuse ={ 0.780392f, 0.568627f, 0.113725f, 1.0f };
final float[] ven_specular ={ 0.992157f, 0.941176f, 0.807843f, 1.0f };
final float ven_shine []= {27.8974f};

//Tierra
final float[] Tie_ambient ={ 0.0f,0.05f,0.05f,1.0f };
final float[] Tie_diffuse ={0.4f,0.5f,0.5f,1.0f };
final float[] Tie_specular ={0.04f,0.7f,0.7f,1.0f };
float shine []= {10.0f};

//Marte
final float[] Mar_ambient ={ 0.1745f, 0.01175f, 0.01175f, 0.55f };
final float[] Mar_diffuse ={0.61424f, 0.04136f, 0.04136f, 0.55f };
final float[] Mar_specular ={0.727811f, 0.626959f, 0.626959f, 0.55f };

//Jupiter
final float[] Jup_ambient ={ 0.2125f, 0.1275f, 0.054f, 1.0f };
final float[] Jup_diffuse ={ 0.714f, 0.4284f, 0.18144f, 1.0f };
final float[] Jup_specular ={ 0.393548f, 0.271906f, 0.166721f, 1.0f };

//Saturno
final float[] Sat_ambient ={ 0.24725f, 0.2245f, 0.0645f, 1.0f };
final float[] Sat_diffuse ={0.34615f, 0.3143f, 0.0903f, 1.0f };
final float[] Sat_specular ={ 0.797357f, 0.723991f, 0.208006f, 1.0f};

//Urano
final float[] Urn_ambient ={ 0.1f, 0.18725f, 0.1745f, 0.8f };
final float[] Urn_diffuse ={0.396f, 0.74151f, 0.69102f, 0.8f };
final float[] Urn_specular ={0.297254f, 0.30829f, 0.306678f, 0.8f };

//Neptuno
final float[] Nep_ambient ={ 0.105882f, 0.058824f, 0.113725f, 1.0f };
final float[] Nep_diffuse ={0.427451f, 0.470588f, 0.541176f, 1.0f };
final float[] Nep_specular ={0.333333f, 0.333333f, 0.521569f, 1.0f };

//Pluton
final float[] Plu_ambient ={ 0.05f,0.05f,0.05f,1.0f };
final float[] Plu_diffuse ={ 0.5f,0.5f,0.5f,1.0f};
final float[] Plu_specular ={ 0.7f,0.7f,0.7f,1.0f};


  final float[] colorBlack  = {0.0f,0.0f,0.0f,1.0f};
  final float[] colorWhite  = {1.0f,1.0f,1.0f,1.0f};
  final float[] colorGray   = {0.4f,0.4f,0.4f,1.0f};
  final float[] colorDarkGray = {0.2f,0.2f,0.2f,1.0f};
  final float[] colorRed    = {1.0f,0.0f,0.0f,1.0f};
  final float[] colorGreen  = {0.0f,1.0f,0.0f,1.0f};
  final float[] colorBlue   = {0.0f,0.0f,0.6f,1.0f};
  final float[] colorYellow = {1.5f,1.5f,0.0f,1.0f};
  final float[] colorLightYellow = {.5f,.5f,0.0f,1.0f};
  ///////////////// Funciones /////////////////////////

  public Light() {
    this.addGLEventListener(this);
    this.addKeyListener(this);
  }

 /////////////// Define Luz y Material /////////

  private GLU glu;  // para las herramientas GL (GL Utilities)
  private GLUT glut;
  
  public void init( GLAutoDrawable drawable )
    {
      GL2 gl = drawable.getGL().getGL2();
      // Establece un material por default.
      gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // set background (clear) color
      gl.glClearDepth(1.0f);      // set clear depth value to farthest
      gl.glEnable(GL_DEPTH_TEST); // enables depth testing
      gl.glDepthFunc(GL_LEQUAL);  // the type of depth test to do 
      gl.glShadeModel(GL_SMOOTH); // blends colors nicely, and smoothes out lighting  
      
      setSomeWhiteMaterial( gl, GL.GL_FRONT_AND_BACK );
      
      // Alguna luz de ambiente global.
      gl.glLightModelfv( GL2.GL_LIGHT_MODEL_AMBIENT, this.ambient, 0 );
      
      // First Switch the lights on.
      gl.glEnable( GL2.GL_LIGHTING );
      //gl.glDisable( GL2.GL_LIGHTING );
      gl.glEnable( GL2.GL_LIGHT0 );
      //gl.glEnable( GL2.GL_LIGHT1 );
      //gl.glEnable( GL2.GL_LIGHT2 );
      //gl.glEnable( GL2.GL_LIGHT3 ); 
      //gl.glEnable( GL2.GL_LIGHT4 ); // Posicional en Origen


      // Light 0.
      //	      
      //
      gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_AMBIENT, ambient, 0 );
      gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_DIFFUSE, colorWhite, 0 );
      gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_POSITION, position, 0 );	

      // Light 1.
      //
      
      gl.glLightfv( GL2.GL_LIGHT1, GL2.GL_AMBIENT, colorWhite, 0 );
      gl.glLightfv( GL2.GL_LIGHT1, GL2.GL_DIFFUSE, colorWhite, 0 );
      gl.glLightfv( GL2.GL_LIGHT1, GL2.GL_SPECULAR, colorWhite, 0 );
      //gl.glLightfv( GL.GL_LIGHT1, GL.GL_SPECULAR, colorRed, 0 );
      //
      gl.glLightf( GL2.GL_LIGHT1, GL2.GL_CONSTANT_ATTENUATION, 0.5f );

      // Light 2.
      //
      gl.glLightfv( GL2.GL_LIGHT2, GL2.GL_AMBIENT, colorBlack, 0 );
      gl.glLightfv( GL2.GL_LIGHT2, GL2.GL_DIFFUSE, colorDarkGray, 0 );
      gl.glLightfv( GL2.GL_LIGHT2, GL2.GL_SPECULAR, colorDarkGray, 0 );
      //
      gl.glLightf( GL2.GL_LIGHT2, GL2.GL_CONSTANT_ATTENUATION, 0.8f );

      // Light 3.
      //
      gl.glLightfv( GL2.GL_LIGHT3, GL2.GL_AMBIENT, colorWhite, 0 );
      gl.glLightfv( GL2.GL_LIGHT3, GL2.GL_DIFFUSE, colorWhite, 0 );
      gl.glLightfv( GL2.GL_LIGHT3, GL2.GL_SPECULAR, colorWhite, 0 );
      //
      gl.glLightf( GL2.GL_LIGHT3, GL2.GL_CONSTANT_ATTENUATION, 0.3f );

      // Light 4.
      //
      //gl.glLightfv( GL.GL_LIGHT4, GL.GL_AMBIENT, colorWhite, 0 );
      //gl.glLightfv( GL.GL_LIGHT4, GL.GL_DIFFUSE, colorWhite, 0 );
      gl.glLightfv( GL2.GL_LIGHT4, GL2.GL_SPECULAR, colorWhite, 0 );
      //
      gl.glLightf( GL2.GL_LIGHT4, GL2.GL_CONSTANT_ATTENUATION, 0.3f );
    
      this.initPosition(gl);
      
      glu = new GLU();                        // get GL Utilities
      glut = new GLUT();
      
      
    }

  public void initPosition( GL2 gl )
    {
      // Establece la posicion y dirección (spotlight) y SPOT_CUTOFF
      //
      //float posLight1[] = { 0f, 0f, 0f, 1.0f };
      float posLight1[] = { 1.0f, 1.f, 1.f, 3.0f };
      //float posLight1[] = { 2f, 4f, 1f, 1.0f };
      //float posLight1[] = { 0f, 5f, -3f, 1.0f };
      //float posLight1[] = { lightX, lightY, lightZ, 1.0f };
      float spotDirection1[] = { 0.0f, -1.f, 0.f };
      gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_POSITION, posLight1, 0 );
      //gl.glLightf( GL2.GL_LIGHT0, GL2.GL_SPOT_CUTOFF, 15.0F);
      //gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_SPOT_DIRECTION, spotDirection1, 0 );
      //gl.glLightf( GL2.GL_LIGHT0, GL2.GL_SPOT_EXPONENT, 50f  );
      
      // Light2
      //
      /*
      float posLight2[] = { .5f, 1.f, 3.f, 0.0f };
      gl.glLightfv( GL2.GL_LIGHT2, GL2.GL_POSITION, posLight2, 0 );

      // Light3
      //
      float posLight3[] = { .5f, 1.f, 3.f, 0.0f };
      gl.glLightfv( GL2.GL_LIGHT3, GL2.GL_POSITION, posLight3, 0 );

      // Light4
      //
      float posLight4[] = { 0f, 0f, 0f, 1f };
      gl.glLightfv( GL2.GL_LIGHT4, GL2.GL_POSITION, posLight4, 0 );
      */
    }


  /////////////// Move light ////////////////////////////

  // Move light 0.
  public void moveLightX( boolean positivDirection ) {
    lightX += positivDirection ? dLight : -dLight;
  }
  public void moveLightY( boolean positivDirection ) {
    lightY += positivDirection ? dLight : -dLight;
  }
  public void moveLightZ( boolean positivDirection ) {
    lightZ += positivDirection ? dLight : -dLight;
  }

  public void animate( GL2 gl, GLU glu , GLUT glut ) 
    {
      float posLight0[] = { lightX, lightY, lightZ, 1.f };
      gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_POSITION, posLight0, 0 );
      drawLight( gl, glu, glut );
      //lightX += 0.003f;
      //lightY += 0.003f;
    }


 /////////////// Define Material /////////////////////

  public void setLightSphereMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorYellow, 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorYellow, 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR,colorYellow, 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 0 );
      //gl.glMaterialfv( face, GL2.GL_EMISSION, colorYellow, 0 );
      //gl.glMaterialfv( face, GL2.GL_EMISSION, colorLightYellow , 0 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0);
    }

public void setLightSphereMaterial2( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorRed, 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorRed, 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR,colorRed, 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 0 );
      //gl.glMaterialfv( face, GL2.GL_EMISSION, colorRed, 0 );
      //gl.glMaterialfv( face, GL2.GL_EMISSION, colorLightYellow , 0 );
      //gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }

  public void setSomeMaterial( GL2 gl, int face, float rgba[], int offset )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, rgba, offset );
      gl.glMaterialfv(face, GL2.GL_DIFFUSE, rgba, offset );
      gl.glMaterialfv(face, GL2.GL_SPECULAR, rgba, offset );
      gl.glMaterialfv(face, GL2.GL_SHININESS, rgba, offset );
      gl.glMateriali( face, GL2.GL_SHININESS, 4 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }

//Material de sol
 public void setSomeSol( GL2 gl, int face )
    {
      gl.glMaterialfv(face, GL2.GL_DIFFUSE, sol_diffuse, 0 );
      gl.glMaterialfv(face, GL2.GL_SPECULAR, sol_specular, 0 );
      gl.glMaterialfv(face, GL2.GL_AMBIENT, sol_ambient, 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 4 );
      
    }
 //Material de  Mercurio
  public void setSomeMercurio( GL2 gl, int face )
    {
      gl.glMaterialfv(face, GL2.GL_DIFFUSE, mer_diffuse, 0 );
      gl.glMaterialfv(face, GL2.GL_SPECULAR, mer_specular, 0 );
      gl.glMaterialfv(face, GL2.GL_AMBIENT, mer_ambient, 0 );
    }

//Material de venus
  public void setSomeVenus( GL2 gl, int face )
    {
      gl.glMaterialfv(face, GL2.GL_DIFFUSE, ven_diffuse, 0 );
      gl.glMaterialfv(face, GL2.GL_SPECULAR, ven_specular, 0 );
      gl.glMaterialfv(face, GL2.GL_AMBIENT, ven_ambient, 0 );
    
    }

//Material de Tierra
  public void setSomeTierra( GL2 gl, int face )
    {
      gl.glMaterialfv(face, GL2.GL_DIFFUSE, Tie_diffuse, 0 );
      gl.glMaterialfv(face, GL2.GL_SPECULAR, Tie_specular, 0 );
      gl.glMaterialfv(face, GL2.GL_AMBIENT, Tie_ambient, 0 );
    
    }

//Material de Marte 
  public void setSomeMarte( GL2 gl, int face )
    {
      gl.glMaterialfv(face, GL2.GL_DIFFUSE, Mar_diffuse, 0 );
      gl.glMaterialfv(face, GL2.GL_SPECULAR, Mar_specular, 0 );
      gl.glMaterialfv(face, GL2.GL_AMBIENT, Mar_ambient, 0 );
    
    }

//Material de Jupiter
  public void setSomeJupiter( GL2 gl, int face )
    {
      gl.glMaterialfv(face, GL2.GL_DIFFUSE, Jup_diffuse, 0 );
      gl.glMaterialfv(face, GL2.GL_SPECULAR, Jup_specular, 0 );
      gl.glMaterialfv(face, GL2.GL_AMBIENT, Jup_ambient, 0 );
    
    }

//Material de Saturno
  public void setSomeSaturno( GL2 gl, int face )
    {
      gl.glMaterialfv(face, GL2.GL_DIFFUSE, Sat_diffuse, 0 );
      gl.glMaterialfv(face, GL2.GL_SPECULAR, Sat_specular, 0 );
      gl.glMaterialfv(face, GL2.GL_AMBIENT, Sat_ambient, 0 );
    
    }

//Material de Urano
  public void setSomeUrano( GL2 gl, int face )
    {
      gl.glMaterialfv(face, GL2.GL_DIFFUSE, Urn_diffuse, 0 );
      gl.glMaterialfv(face, GL2.GL_SPECULAR, Urn_specular, 0 );
      gl.glMaterialfv(face, GL2.GL_AMBIENT, Urn_ambient, 0 );
    
    }

//Material de Neptuno
  public void setSomeNeptuno( GL2 gl, int face )
    {
      gl.glMaterialfv(face, GL2.GL_DIFFUSE, Nep_diffuse, 0 );
      gl.glMaterialfv(face, GL2.GL_SPECULAR, Nep_specular, 0 );
      gl.glMaterialfv(face, GL2.GL_AMBIENT, Nep_ambient, 0 );
    
    }

//Material de Pluton
  public void setSomePluton( GL2 gl, int face )
    {
      gl.glMaterialfv(face, GL2.GL_DIFFUSE, Plu_diffuse, 0 );
      gl.glMaterialfv(face, GL2.GL_SPECULAR, Plu_specular, 0 );
      gl.glMaterialfv(face, GL2.GL_AMBIENT, Plu_ambient, 0 );
    
    }

  public void setSomeWhiteMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorWhite , 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorWhite , 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorWhite , 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, -100 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }

  public void setSomeGrayMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorGray , 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorGray , 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorGray , 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 0 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }

  public void setSomeDarkGrayMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorDarkGray , 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorDarkGray , 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorDarkGray , 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 4 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }


  public void setSomeYellowMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorBlack , 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorLightYellow, 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorYellow , 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, -50 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }

  public void setSomeBlueMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorBlue , 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorBlue, 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorBlue , 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 4 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }

  public void setSomeRedMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorRed , 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorRed , 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorRed , 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 4 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorBlack , 0 );
    }

  public void setSomeGreenMaterial( GL2 gl, int face )
    {
      gl.glMaterialfv( face, GL2.GL_AMBIENT, colorDarkGray , 0 );
      gl.glMaterialfv( face, GL2.GL_DIFFUSE, colorGreen , 0 );
      gl.glMaterialfv( face, GL2.GL_SPECULAR, colorGreen , 0 );
      gl.glMateriali( face, GL2.GL_SHININESS, 10 );
      gl.glMaterialfv( face, GL2.GL_EMISSION, colorDarkGray , 0 );
    }

  /////////////////// dibujos /////////////////////////

  ///////////////// Dibuja una Esfera con Luz ///////////////


  public void drawLight( GL2 gl, GLU glu , GLUT glut ) 
    {
      setLightSphereMaterial( gl, GL.GL_FRONT_AND_BACK );
        gl.glPushMatrix(); {
	//gl.glTranslatef( lightX, lightY, lightZ );
	//glut.glutSolidSphere( .1f, 100, 100 );
         } gl.glPopMatrix();
    }
  public void drawTeaPotWithLight( GL2 gl, GLUT glut ) 
    {
      //gl.glRotatef(rotX,1.0f, 0.0f, 0.0f);
      glut.glutSolidTeapot( 1.0f, true );
    }

  public void Sol(GL2 gl, GLUT glut ){
      //gl.glRotatef(this.rotacion, rotX, rotY, rotZ);
       //this.rotacion+=5.0f;
       gl.glPushMatrix();
        this.setSomeSol(gl, GL.GL_FRONT_AND_BACK ); 
        if (this.rotacion>360){
            this.rotacion = 0;
        }
      glut.glutSolidSphere(1,100,100);
      gl.glPopMatrix();
  }
  
  public void Mercurio(GL2 gl, GLUT glut ){
      
      gl.glPushMatrix();
      this.setSomeMercurio(gl, GL.GL_FRONT_AND_BACK );  
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
       this.rotacion+=1f;
        gl.glTranslatef(0.0f, 0.0f, 1.5f );
        if (this.rotacion>360){
            this.rotacion = 0;
        }
        glut.glutSolidSphere(.3,100,100);
        gl.glPopMatrix();
  }
    
     public void Venus(GL2 gl, GLUT glut ){
        gl.glPushMatrix(); 
      this.setSomeVenus(gl, GL.GL_FRONT_AND_BACK );  
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
       rotacion+=1f;
        gl.glTranslatef(0f, 0.0f, 2.0f );
        if (this.rotacion>360){
            this.rotacion = 0;
        }
        glut.glutSolidSphere(.3,100,100);
         gl.glPopMatrix();
  }
   
      public void Tierra(GL2 gl, GLUT glut ){
        gl.glPushMatrix();
this.setSomeTierra(gl, GL.GL_FRONT_AND_BACK );   
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      rotacion+=1f;
        gl.glTranslatef(0.0f, 0.0f, 3f );
        if (this.rotacion>360){
            this.rotacion =0;
        }
         glut.glutSolidSphere(.4,100,100);
         gl.glPopMatrix();
  }   
        public void Marte(GL2 gl, GLUT glut ){
        gl.glPushMatrix();
 this.setSomeMarte(gl, GL.GL_FRONT_AND_BACK );  
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      rotacion+=1f;
        gl.glTranslatef(0.0f, 0.0f, 4f );
        if (this.rotacion>360){
            this.rotacion =0;
        }
         glut.glutSolidSphere(.3,10,10);
         gl.glPopMatrix();
  }
        
           public void Jupiter(GL2 gl, GLUT glut ){
        gl.glPushMatrix();
this.setSomeJupiter(gl, GL.GL_FRONT_AND_BACK );   
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      rotacion+=1f;
        gl.glTranslatef(0.0f, 0.0f,5f );
        if (this.rotacion>360){
            this.rotacion =0;
        }
         glut.glutSolidSphere(.6,10,10);
         gl.glPopMatrix();
  }
          
                public void Saturno(GL2 gl, GLUT glut ){
      gl.glPushMatrix();
  this.setSomeSaturno(gl, GL.GL_FRONT_AND_BACK );
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      rotacion+=1f;
        gl.glTranslatef(0.0f, 0.0f, 6f );
        if (this.rotacion>360){
            this.rotacion =0;
        }
         glut.glutSolidSphere(.5,10,10);
         gl.glPopMatrix();
  }
                
     public void Urano(GL2 gl, GLUT glut ){
        gl.glPushMatrix();
 this.setSomeUrano(gl, GL.GL_FRONT_AND_BACK );   
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      rotacion+=1f;
        gl.glTranslatef(0.0f, 0.0f, 7f );
        if (this.rotacion>360){
            this.rotacion =0;
        }
         glut.glutSolidSphere(.3,10,10);
         gl.glPopMatrix();
  }
     
    public void Neptuno(GL2 gl, GLUT glut ){
      gl.glPushMatrix();
this.setSomeNeptuno(gl, GL.GL_FRONT_AND_BACK );      
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      rotacion+=1f;
        gl.glTranslatef(0.0f, 0.0f, 8f );
        if (this.rotacion>360){
            this.rotacion =0;
        }
         glut.glutSolidSphere(.4,10,10);
         gl.glPopMatrix();
  }
      public void Pluton(GL2 gl, GLUT glut ){
      gl.glPushMatrix();
this.setSomePluton(gl, GL.GL_FRONT_AND_BACK );  
      gl.glRotatef(this.rotacion, 0.0f, 1.0f, 0.0f);
      rotacion+=-9f;
        gl.glTranslatef(0.0f, 0.0f, 9f );
        if (this.rotacion>360){
            this.rotacion =0;
        }
         glut.glutSolidSphere(.1,10,10);
         gl.glPopMatrix();
  }

 
    @Override
    public void dispose(GLAutoDrawable glad) {
      
    }

    @Override
    public void display(GLAutoDrawable glad) {
        
        GL2 gl = glad.getGL().getGL2();  // get the OpenGL 2 graphics context
        gl.glMatrixMode(GL_MODELVIEW);
        gl.glLoadIdentity();  // reset the model-view matrix
 
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        glu.gluLookAt(10.0, 10.0, 10.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
        
        //this.setLightSphereMaterial2(gl,GL.GL_FRONT_AND_BACK);
  
        this.Sol(gl, glut);    
        this.Mercurio(gl, glut);   
        this.Venus(gl, glut); 
        this.Tierra(gl, glut);
        this.Marte(gl, glut);
        this.Jupiter(gl, glut);
        this.Saturno(gl, glut);
        this.Urano(gl, glut);
        this.Neptuno(gl, glut);
        this.Pluton(gl, glut);
        
        this.animate(gl,this.glu,this.glut);
        
       
    }

    @Override
    public void reshape(GLAutoDrawable glad,int x, int y, int width, int height) {
        GL2 gl = glad.getGL().getGL2();  // get the OpenGL 2 graphics context

        if (height == 0) height = 1;   // prevent divide by zero
        float aspect = (float)width / height;

        // Set the view port (display area) to cover the entire window
        gl.glViewport(0, 0, width, height);

        // Setup perspective projection, with aspect ratio matches viewport
        gl.glMatrixMode(GL_PROJECTION);  // choose projection matrix
        gl.glLoadIdentity();             // reset projection matrix
        glu.gluPerspective(fovy, aspect, 0.1, 50.0); // fovy, aspect, zNear, zFar
        
        // Enable the model-view transform
        gl.glMatrixMode(GL_MODELVIEW);
        gl.glLoadIdentity(); // reset
        
    }

       public static void main(String[] args) {
      // Run the GUI codes in the event-dispatching thread for thread safety
      SwingUtilities.invokeLater(new Runnable() {
         @Override
         public void run() {
            // Create the OpenGL rendering canvas
            GLCanvas canvas = new Light();
            canvas.setPreferredSize(new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT));
 
            // Create a animator that drives canvas' display() at the specified FPS.
            final FPSAnimator animator = new FPSAnimator(canvas, FPS, true);
 
            // Create the top-level container
            final JFrame frame = new JFrame(); // Swing's JFrame or AWT's Frame
            JPanel panel1 = new JPanel();
            JPanel panel2 = new JPanel();

            FlowLayout fl = new FlowLayout();
            frame.setLayout(fl);
            
            panel1.add(canvas);
            frame.getContentPane().add(panel1);
            frame.getContentPane().add(panel2);
            frame.addKeyListener((KeyListener) canvas);
            
            frame.addWindowListener(new WindowAdapter() {
               @Override
               public void windowClosing(WindowEvent e) {
                  // Use a dedicate thread to run the stop() to ensure that the
                  // animator stops before program exits.
                  new Thread() {
                     @Override
                     public void run() {
                        if (animator.isStarted()) animator.stop();
                        System.exit(0);
                     }
                  }.start();
               }
            });
                        
            frame.setTitle(TITLE);
            frame.pack();
            frame.setVisible(true);
            animator.start(); // start the animation loop
      
         }
      });
   }

    @Override
    public void keyTyped(KeyEvent e) {
 
    }

    @Override
    public void keyPressed(KeyEvent e) {
       int codigo = e.getKeyCode();
        //  lightX, lightY, lightZ
        System.out.println("codigo presionado = "+codigo);
        
        switch (codigo){
            case KeyEvent.VK_DOWN : this.moveLightY(true); break;
            case KeyEvent.VK_UP   : this.moveLightY(false); break;            
            case KeyEvent.VK_RIGHT : this.moveLightX(true); break;
            case KeyEvent.VK_LEFT  : this.moveLightX(false); break;            
            case KeyEvent.VK_PAGE_UP : this.moveLightZ(false); break;
            case KeyEvent.VK_PAGE_DOWN : this.moveLightZ(true); break;             
        }
        System.out.println("rotX = "+rotX);
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
    

}